# Gitlab
It is developed on the basis of Git version control system and is the one of the best code hosting platforms. It is similar to github. In addition to hosting of your code, it has the services that provide features to manage software development lifecycle. Some of these features are listed below.
 ### Features of Gitlab
 - Free service.
 - Open source code repository.
 - Free hosting.
 - Bug tracking mechanism.
 - Editing of files in web interface.
 - Sharing of code with different people.
